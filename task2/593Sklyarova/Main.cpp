#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "LightInfo.hpp"
#include "Texture.hpp"
#include "Camera.hpp"

#include <iostream>
#include <vector>


#include <glm/glm.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>


#include <algorithm>
#include <cmath>
#include <stdlib.h>
#include <algorithm>
#include <SOIL2.h>



using namespace std;


class HeightMap {
public:

    vector<float> heightMap;
    int width;
    int height;
    int channels;

    float maxHeight;
    float minHeight;

    HeightMap(const char* filename, float maxH, float minH) {

        this->maxHeight = maxH;
        this->minHeight = minH;


        unsigned char* htMap = SOIL_load_image
                (
                        filename,
                        &width, &height, &channels,
                        SOIL_LOAD_L
                );

        for (int i = 0; i < width*height; i++) {
            heightMap.push_back((float)(htMap[i]));
        }

        float maxHeight = *( max_element(std::begin(heightMap), std::end(heightMap)) );
        float minHeight = *( min_element(std::begin(heightMap), std::end(heightMap)) );

        for (int i = 0; i < width*height; i++) {
            heightMap[i] = minH + (maxH - minH) * (heightMap[i] - minHeight) / (maxHeight- minHeight);
        }

    }

};


struct Vertex {
    glm::vec3 coords;
    glm::vec3 normal;

    Vertex() {
        coords = glm::vec3(0.0, 0.0, 0.0);
        normal = glm::vec3(0.0, 1.0, 0.0);
    }
    Vertex(int i, int j, float height, float step) {
        coords.x = j * step;
        coords.y = height;
        coords.z = i * step;
    }


};

class Terrain {
private:
    int width;
    int length;
    float step;
    float minHeight;
    float maxHeight;

    Vertex** vertices;



    void computeNormals() {
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                vertices[i][j].normal = glm::vec3(0.0, 0.0, 0.0);

                glm::vec3 up, down, right, left;
                if (i > 0) {
                    up = glm::vec3(0.0, vertices[i - 1][j].coords.y - vertices[i][j].coords.y, -1.0);
                }
                if (i < length - 1) {
                     down = glm::vec3(0.0, vertices[i + 1][j].coords.y - vertices[i][j].coords.y, 1.0);
                }
                if (j > 0) {
                    left = glm::vec3(-1.0, vertices[i][j - 1].coords.y - vertices[i][j].coords.y, 0.0);
                }
                if (j < width - 1) {
                    right = glm::vec3(1.0, vertices[i][j + 1].coords.y - vertices[i][j].coords.y, 0.0);
                }

                if (j > 0 && i > 0) {
                    vertices[i][j].normal += glm::normalize(glm::cross(up, left));
                }
                if (j > 0 && i < length - 1) {
                    vertices[i][j].normal += glm::normalize(glm::cross(left, down));
                }
                if (j < width - 1 && i < length - 1) {
                    vertices[i][j].normal += glm::normalize(glm::cross(down, right));
                }
                if (j < width - 1 && i > 0) {
                    vertices[i][j].normal += glm::normalize(glm::cross(right, up));
                }

                vertices[i][j].normal = glm::normalize(vertices[i][j].normal);

            }
        }
    }

public:


    Terrain(HeightMap* heightMap,  float step) {
        width = heightMap->width;
        length = heightMap->height;

        this->step = step;
        minHeight = heightMap->minHeight;
        maxHeight = heightMap->maxHeight;

        vertices = new Vertex*[length];
        for (int i = 0; i < length; i++) {
            vertices[i] = new Vertex[width];
            for (int j = 0; j < width; j++) {
                vertices[i][j] = Vertex(i, j, (heightMap->heightMap[i * width + j]), step);
            }
        }

        computeNormals();
    }

    ~Terrain() {
        for (int i = 0; i < length; i++) {
            delete[] vertices[i];
        }
        delete[] vertices;
    }

    int getWidth() {
        return width * step;
    }

    int getLength() {
        return length * step;
    }

    float getMaxHeight(){
        return maxHeight;
    }

    float getMinHeight(){
        return minHeight;
    }

    float getHeight(int i, int j) {
        return vertices[i][j].coords.y;
    }

    float getX(int i, int j) {
        return vertices[i][j].coords.x;
    }

    float getY(int i, int j) {
        return vertices[i][j].coords.z;
    }


    float getStep() {
        return step;
    }


    MeshPtr getMesh()
    {
        std::vector<glm::vec3> verts;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> textures;

        int N = 2;

        for (int i = 0; i < length - 1; i++) {
            for (int j = 0; j < width - 1; j++) {

                glm::vec3 normal = vertices[i][j].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i][j].coords);
                textures.push_back(glm::vec2(0.0, 0.0));
                //textures.push_back(glm::vec2((i)%N, (j)%N));


                normal = vertices[i][j + 1].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i][j + 1].coords);
                textures.push_back(glm::vec2(0.0, 1.0));
                //textures.push_back(glm::vec2((i)%N, (j+1)%N));


                normal = vertices[i + 1][j].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i + 1][j].coords);
                textures.push_back(glm::vec2(1.0, 0.0));
                //textures.push_back(glm::vec2((i+1)%N, (j)%N));

                normal = vertices[i][j + 1].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i][j + 1].coords);
                textures.push_back(glm::vec2(0.0, 1.0));
                //textures.push_back(glm::vec2((i)%N, (j+1)%N));

                normal = vertices[i + 1][j].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i + 1][j].coords);
                textures.push_back(glm::vec2(1.0, 0.0));
                //textures.push_back(glm::vec2((i+1)%N, (j)%N));

                normal = vertices[i + 1][j + 1].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i + 1][j + 1].coords);
                textures.push_back(glm::vec2(1.0, 1.0));
                //textures.push_back(glm::vec2((i+1)%N, (j+1)%N));

            }
        }

        /////////////////////////////////////////
       for(int i = 0; i < verts.size(); i++){
            swap(verts[i].y,  verts[i].z);
            swap(normals[i].y,  normals[i].z);
       }
        ////////////////////////////////////////

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(verts.size() * sizeof(float) * 3, verts.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(textures.size() * sizeof(float) * 2, textures.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);

        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(verts.size());


        return mesh;
    }
};



class FPVCamera: public FreeCameraMover {

    Terrain * terrain;
public:

    FPVCamera(glm::vec3 pos, Terrain * terrain) :
            FreeCameraMover(pos) {
        this->terrain = terrain;
        correctHeight();
    }


    void update(GLFWwindow* window, double dt)
    {
        float speed = 1.0f;

        //Получаем текущее направление "вперед" в мировой системе координат
        glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

        //Получаем текущее направление "вправо" в мировой системе координат
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

        //Двигаем камеру вперед/назад
        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        {
            _pos += forwDir * speed * static_cast<float>(dt);
        }
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        {
            _pos -= forwDir * speed * static_cast<float>(dt);
        }
        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        {
            _pos -= rightDir * speed * static_cast<float>(dt);
        }
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
            _pos += rightDir * speed * static_cast<float>(dt);
        }

        correctHeight();

        //-----------------------------------------

        //Соединяем перемещение и поворот вместе
        _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

        //-----------------------------------------



        int width, height;
        glfwGetFramebufferSize(window, &width, &height);

        //Обновляем матрицу проекции на случай, если размеры окна изменились
        _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
    }


    void correctHeight() {
        int i = (_pos.x)/terrain->getStep();
        int j = (_pos.y)/terrain->getStep();
       std::cout << "pos " << _pos.x << " " << _pos.y <<  " " << _pos.z << std::endl;
       std::cout << terrain-> getX(j,i) << " " << terrain-> getY(j,i)  <<  " " << terrain-> getHeight(j,i)  << std::endl;

        _pos.z = terrain->getHeight(j, i)+0.2;

    }
};


class SampleApplication : public Application
{
public:
    Terrain* Terr;
    MeshPtr _terrain;

    //Координаты источника света
    float _phi = 1.42f;
    float _theta = glm::pi<float>() * 0.2f;


    //Параметры источника света
    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;
    glm::vec3 _lightSpecularColor;

    TexturePtr _grassTex;
    TexturePtr _snowTex;
    TexturePtr _rockTex;
    TexturePtr _waterTex;

    GLuint _sampler;

    ShaderProgramPtr _shader;



    SampleApplication(Terrain* terrain) {
        Terr = terrain;
    }


    void makeScene() override
    {
        Application::makeScene();

       _cameraMover = std::make_shared<FPVCamera>(
                glm::vec3(
                        Terr->getLength() / 2.0f,
                        Terr->getWidth() / 2.0f,
                        Terr->getMaxHeight()* 1.05f),
                        Terr
        );


        _terrain  = Terr->getMesh();

        //Создание и загрузка мешей
        _terrain->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));



        _shader = std::make_shared<ShaderProgram>("593SklyarovaData2/common.vert", "593SklyarovaData2/common.frag");

        //Инициализация значений переменных освщения
        _lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
        _lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
        _lightSpecularColor = glm::vec3(1.0, 1.0, 1.0);


        _grassTex=loadTexture("593SklyarovaData2/textures/grass.jpg");
        _snowTex=loadTexture("593SklyarovaData2/textures/snow.png");
        _rockTex=loadTexture("593SklyarovaData2/textures/rock.png");
        _waterTex=loadTexture("593SklyarovaData2/textures/water.jpg");

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



        _shader->use();


        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setFloatUniform("maxHeight", Terr->getMaxHeight());
        _shader->setFloatUniform("minHeight", Terr->getMinHeight());

        glm::vec3 lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));



        _shader->setVec3Uniform("dir", lightDir);
        _shader->setVec3Uniform("light.La", _lightAmbientColor);
        _shader->setVec3Uniform("light.Ld", _lightDiffuseColor);
        _shader->setVec3Uniform("light.Ls", _lightSpecularColor);




        GLuint textureUnitGrass = 0;
        GLuint textureUnitSnow = 1;
        GLuint textureUnitRock = 2;
        GLuint textureUnitWater = 3;

        bind(_grassTex, textureUnitGrass, "grassTex");
        bind(_snowTex, textureUnitSnow, "snowTex");
        bind(_rockTex, textureUnitRock, "rockTex");
        bind(_waterTex, textureUnitWater, "waterTex");




        _shader->setMat4Uniform("modelMatrix", _terrain->modelMatrix());

        _shader->setMat3Uniform("normalToCameraMatrix",
                                glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _terrain->modelMatrix()))));
        _terrain->draw();


        glBindSampler(0, 0);
        glUseProgram(0);

    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_lightAmbientColor));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_lightDiffuseColor));
                ImGui::ColorEdit3("specular", glm::value_ptr(_lightSpecularColor));

                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void bind(TexturePtr texture, GLuint index, std::string name) {
        glBindTextureUnit(index, texture->texture());
        glBindSampler(index, _sampler);
        _shader->setIntUniform(name, index);
    }

 };




int main()
{
    float step = 0.1;
    HeightMap _heightMap = HeightMap("593SklyarovaData2/tamriel.jpg", 1.5, 0);
    Terrain* _terrain = new Terrain(&_heightMap, step);

    SampleApplication app(_terrain);
    app.start();

    return 0;
}