#version 330

uniform sampler2D grassTex;
uniform sampler2D snowTex;
uniform sampler2D rockTex;
uniform sampler2D waterTex;


struct LightInfo
{
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

in vec4 lightDirCamSpace;
in float height;



out vec4 fragColor; //выходной цвет фрагмента

const float shininess = 128.0;


float normalDistr(float x, float mu, float sigma) {
	return  exp(-((x - mu) * (x - mu) / (2 * sigma * sigma)));
}


void main()
{
	vec4 blending = vec4(normalDistr(height, 0.14, 0.07),
					normalDistr(height, 1.0, 0.25 ),
					normalDistr(height, 0.48, 0.14),
					normalDistr(height, 0.0, 0.07)
					);

	vec3 diffuseColor = texture(grassTex, texCoord).rgb * blending[0] +
	 texture(snowTex, texCoord).rgb * blending[1] +
	texture(rockTex, texCoord).rgb * blending[2] +
	texture(waterTex, texCoord).rgb * blending[3];



	vec3 Ks = dot(blending, vec4(0, 0.6, 0.2, 0.8)) * vec3(1);


	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	vec3 color = diffuseColor * (light.La + light.Ld * NdotL);

	if (NdotL > 0.0)
	{
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		color += light.Ls * Ks * blinnTerm;
	}

	fragColor = vec4(color, 1.0);
}
